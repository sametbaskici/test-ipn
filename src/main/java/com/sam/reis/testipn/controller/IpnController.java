package com.sam.reis.testipn.controller;

import com.sam.reis.testipn.model.PayIpn;
import com.sam.reis.testipn.model.PayoutIpn;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;

@RequestMapping("/ipn")
@RestController
public class IpnController {

    @PostMapping("/pay")
    public ResponseEntity handlePay(@RequestBody PayIpn payIpn){
        return getResponse();
    }

    @PostMapping("/payout")
    public ResponseEntity handlePayout(@RequestBody PayoutIpn payoutIpn){
        return getResponse();
    }

    private ResponseEntity getResponse(){
        int random =  new Random().nextInt(4);
        switch (random){
            case 0 : return new ResponseEntity(HttpStatus.OK);

            case 1 : return new ResponseEntity(HttpStatus.BAD_REQUEST);

            case 2 :  return new ResponseEntity(HttpStatus.NOT_FOUND);

            case 3 : return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return null;
    }
}
