package com.sam.reis.testipn.model;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class PayoutIpn {
    private String payoutId;
    private String orderId;
    private String customer;
    private String type;
    private String status;
    private BigDecimal amount;
    private String currency;
}
