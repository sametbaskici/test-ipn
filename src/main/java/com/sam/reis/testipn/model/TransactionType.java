package com.sam.reis.testipn.model;

public enum TransactionType {
    TRANSFER,
    PAY,
    PAYOUT,
    EXCHANGE,
    WITHDRAW,
    DEPOSIT,
    INCREASE_BALANCE,
    DECREASE_BALANCE
}
