package com.sam.reis.testipn.model;

public enum TransactionStatus {
    WAITING, DECLINED, APPROVED, PENDING, CANCELED;
}
