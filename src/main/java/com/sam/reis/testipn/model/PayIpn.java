package com.sam.reis.testipn.model;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class PayIpn {
    private String paymentId;
    private String orderId;
    private String customer;
    private String type;
    private String status;
    private String paymentJourneyStatus;
    private BigDecimal amount;
    private String currency;
}