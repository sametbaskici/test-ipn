package com.sam.reis.testipn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestIpnApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestIpnApplication.class, args);
    }
}
